import logo from './logo.svg';
import './style.css'
export const Content = (props) => {
  const { list, text, changeText, clickAdd, clickDelete, type, disabled} = props
  return (
    <div className='content'>
      <div className='head'>
        <div>
          <img src={logo} className="logo" alt="logo" />
        </div>
        <div>
          Todo list
        </div>
        <div>
          <img src={logo} className="logo" alt="logo" />
        </div>
      </div>
      <div className='input-text'>
        <input className='input' value={text} onChange={changeText} onKeyUp={changeText} maxLength={20} disabled={disabled}/>
        <div className={`btn-add ${disabled ? 'disable':''}`} onClick={clickAdd}> Add </div>
      </div>
      <div>
        {
          list.map((text, i) => {
            return (
              <div key={`text-${i}`} className='list-text'>
                <div className='text'>
                  {text}
                </div>
                <div>
                  <div className='btn-delete' onClick={() => clickDelete(i)}> Delete </div>
                </div>
              </div>
            )
          })
        }
      </div>
      <div className='description'>
        This content create code by {type}.
      </div>
    </div>
  )
}
