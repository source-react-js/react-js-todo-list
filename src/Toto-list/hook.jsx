import { useState } from "react"
import { Content } from './content';
export const TodoList = () => {

  const [text, setText] = useState('')
  const [list, setList] = useState([])

  const clickAdd = () => {
    if (text) {
      setText('')
      setList([...list, text])
    }
  }

  const clickDelete = (index) => {
    setList(list.filter((e, i) => i !== index))
  }

  const changeText = (element) => {
    if (element.key === "Enter") {
      setText('')
      clickAdd()
    } else {
      setText(element.target.value)
    }
  }

  return (
    <Content text={text} list={list} changeText={changeText} clickAdd={clickAdd} clickDelete={clickDelete} disabled={list.length === 10} type='Hook'/>
  )
}

export default TodoList