import React, { Component } from "react"
import { Content } from './content';
export class TodoList extends Component {
  constructor() {
    super()
    this.state = {
      text: '',
      list: []
    }
  }

  clickAdd = () => {
    const { list, text } = this.state
    if (text) {
      this.setState({
        list: [...list, text],
        text: ''
      })
    }
  }

  clickDelete = (index) => {
    const { list } = this.state
    this.setState({
      list: list.filter((e, i) => i !== index),
    })
  }

  changeText = (element) => {
    if (element.key === "Enter") {
      this.setState({
        text: element.target.value
      }, () => {
        this.clickAdd()
      })
    } else {
      this.setState({
        text: element.target.value
      })
    }
  }

  render() {
    const { list, text } = this.state
    return (
      <Content text={text} list={list} changeText={this.changeText} clickAdd={this.clickAdd} clickDelete={this.clickDelete} disabled={list.length === 10} type='Class'/>
    )
  }
}

export default TodoList