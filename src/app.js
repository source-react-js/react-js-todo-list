import './app.css'
import Hook from './Toto-list/hook.jsx'
import Class from './Toto-list/class.jsx'
export const App = () => {
  return (
    <div className="app">
      <Hook/>
      <Class/>
    </div>
  )
}
